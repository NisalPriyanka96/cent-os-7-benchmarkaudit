#!/bin/bash

echo "************* CENT OS 7 COMPLIANCE AUDIT **************" >> audit.txt
echo -e "\n" >> audit.txt
echo "************* BENCHMARK VERSION : CIS CentOS Linux 7 Benchmark *************" >> audit.txt
echo -e "\n" >> audit.txt

## input audit rules list 
    # AUDIT RULE FORMAT #
    ##      rule1 command1
    ###     rule1 command2
    ####    \n
    #####   rule2 command1

input="rules.txt"   


## read and exute audit commands line by line
while IFS= read -r line
do
    if [ -z "$line" ];
    then
         echo "******************************">> audit.txt
         echo -e "\n" >> audit.txt
         continue
    else
         echo "command : $line" >> audit.txt
         echo "Result : $(eval $line)" >> audit.txt
         continue
    fi
   
done < $input

## running ext-scripts
echo "READING EXT AUDIT CHECKS.."

for file in EXT_SCRIPTS/*
do
    echo "reading $file"
    bash $file > $file.txt
done
